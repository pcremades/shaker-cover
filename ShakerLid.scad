// Tapa de shaker Lorena Carvelli
// This is a housing for the LED driver and battery, to be used in place of the
// third lamp of the spectrophotometer.
//
// Copyright (c) 2021 Pablo Cremades
//
// The MIT License (MIT)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
// and associated documentation files (the "Software"), to deal in the Software without restriction, 
// including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do 
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial
// portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

$fn=50;
//dimensionsTop = [150, 99];
topWidthInside = 150;
topLengthInside = 100;
topHeightInside = 35; //33.5
topCornerRadius = 6;

bottomLengthInside = 115;
bottomWidthInside = 165;
bottomHeightInside = 9.5;
bottomCornerRadius = 20;

sideApertureWidth = 10;
sideApertureHeight = 20;
sideAperturePos = 16;
wallThickness = 2.2;

ependorfDiam = 11.5;
ependorfSepX = 10.7 + ependorfDiam;
ependorfSepY = 13.7 + ependorfDiam;

screwHeadDiam = 9.3;
screwHeadHeight = 3.5;
screwDiam = 3.2;  //It's 3mm + 0.2 tolerance 
screwSepX = 103;
screwSepY = 50.8;
screwPositions = [[screwSepX/2, screwSepY/2],
		  [-screwSepX/2, screwSepY/2],
		  [-screwSepX/2, -screwSepY/2],
		  [screwSepX/2, -screwSepY/2]];

module corners(x, y, z, h, r){
     translate([x, y, z]) cylinder(r=r, h=h);
     translate([-x, y, z]) cylinder(r=r, h=h);
     translate([-x, -y, z]) cylinder(r=r, h=h);
     translate([x, -y, z]) cylinder(r=r, h=h);
     }


module topLid(){
     difference(){
//Box
	  hull(){
	       corners(x=topWidthInside/2 - topCornerRadius + wallThickness,
		       y=topLengthInside/2 - topCornerRadius + wallThickness,,
		       z=0,
		       h=topHeightInside-bottomHeightInside,
		       r=topCornerRadius);
	  }

//Inside
	  hull(){
	       corners(x=topWidthInside/2 - topCornerRadius,
		       y=topLengthInside/2 - topCornerRadius,
		       z=wallThickness,
		       h=topHeightInside,
		       r=topCornerRadius);
	  }
     }
}

module ependorf(){
     diameter = ependorfDiam;
     chaflan = 15;
     cylinder(h=wallThickness*1.2, d1=chaflan, d2=diameter);
     }

module ependorfHoles(){
     for( mirror = [0:1] ){
	  mirrorDist = 69.8 + ependorfDiam;
	  for( i = [0:2] ){
	       for( j = [0:3] ){
		    translate( [i*ependorfSepX + mirror*mirrorDist, j*ependorfSepY, -wallThickness*0.1] ) ependorf();
	       }
	  }
     }
}

module screwHole(){
     difference(){
	  difference(){
	       cylinder(d=screwHeadDiam+wallThickness*2, h=screwHeadHeight+wallThickness);
	       translate([0, 0, -wallThickness*0.1]) cylinder(d=screwHeadDiam, h=screwHeadHeight);
	  }
	  translate([0, 0, -5]) cylinder(d=screwDiam, h=100);
     }
}

module screwPlacement(){
     for( i = screwPositions ){
	  translate(i) screwHole();
     }
}

module bottomLid(){
     difference(){
//Box
	  hull(){
	       hull(){
		    corners(x=bottomWidthInside/2 - bottomCornerRadius + wallThickness,
			    y=bottomLengthInside/2 - bottomCornerRadius + wallThickness,,
			    z=0,
			    h=bottomHeightInside,
			    r=bottomCornerRadius);
	       }
	       hull(){
		    corners(x=topWidthInside/2 - topCornerRadius + wallThickness,
			    y=topLengthInside/2 - topCornerRadius + wallThickness,,
			    z=-bottomHeightInside,
			    h=bottomHeightInside,
			    r=topCornerRadius);
	       }
	  }
	  
//Inside
	  hull(){
	  translate([0, 0, -0.1]){
	       hull(){
		    corners(x=bottomWidthInside/2 - bottomCornerRadius,
			    y=bottomLengthInside/2 - bottomCornerRadius,
			    z=0,
			    h=bottomHeightInside*1.2,
			    r=bottomCornerRadius);
	       }
	  }
	  translate([0, 0, -0.1]){
	       hull(){
		    corners(x=topWidthInside/2 - topCornerRadius-1.2, //Ni idea por qué 1.2
			    y=topLengthInside/2 - topCornerRadius-1.2,
			    z=-bottomHeightInside*1.1,
			    h=topHeightInside*1.2,
			    r=topCornerRadius);
	       }
	  }
	  
	  }
     } //diff
}


difference(){
     union(){
	  difference(){
	       difference(){
		    topLid();
		    translate( [-(113.5+ependorfDiam)/2, -(64+ependorfDiam)/2, 0] ) ependorfHoles();
	       }
	       for( i = screwPositions ){
		    translate([0,0,-screwHeadHeight]) translate(i) cylinder(d=screwHeadDiam, h=screwHeadHeight*3);
	       }
	  }
	  screwPlacement();
	  translate([0, 0, topHeightInside*0.99]) bottomLid();
     }
     
     union(){
	  translate([topWidthInside/2-sideApertureWidth/2-sideAperturePos,0,sideAperturePos]){
	       cube([sideApertureWidth, 120, sideApertureHeight], center=true);
	  }
	  
	  translate([-(topWidthInside/2-sideApertureWidth/2-sideAperturePos),0,sideAperturePos]){
	       cube([sideApertureWidth, 120, sideApertureHeight], center=true);
	  }
     }
}
