# Shaker Cover
Repuesto de la tapa del **ThermoMixer 5436** para tubos ependorf.

![Shaker Lid Render](img/ShakerLid_1.png)

- Desarrollado en [OpenSCAD](http://openscad.org).
- Algunos conceptos de diseños tomados de [aquí](https://www.youtube.com/watch?v=i-zH55B-b8Y).
- [Perfil de Ultimaker CURA v4.9 para impresión en PETG](PETG.curaprofile)

**Nota:** se recomienda colocar [arandelas](Arandela.stl) impresas en Nylon en cada tornillo
entre la tapa y el bloque de metal, para evitar la exposición directa del PETG a altas temperaturas.

### Fotos del modelo terminado, impreso en PETG
<img src="img/perspective_1.jpg" alt="drawing" width="200"/>
<img src="img/top.jpg" alt="drawing" width="200"/>
<img src="img/perspective_1.jpg" alt="drawing" width="200"/>

