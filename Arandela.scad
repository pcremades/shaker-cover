// Arandela para la tapa de shaker Lorena Carvelli. Imprimir en Nylon
// This is a housing for the LED driver and battery, to be used in place of the
// third lamp of the spectrophotometer.
//
// Copyright (c) 2021 Pablo Cremades
//
// The MIT License (MIT)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
// and associated documentation files (the "Software"), to deal in the Software without restriction, 
// including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do 
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial
// portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

$fn=50;
     
screwHeadDiam = 9.3;
washerHeight = 1.2;
screwDiam = 3.2;  //It's 3mm + 0.2 tolerance 

difference(){
     cylinder( d=screwHeadDiam, h=washerHeight);
     translate([0,0,-washerHeight]) cylinder( d=screwDiam, h=washerHeight*3);
}
